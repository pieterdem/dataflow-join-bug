package dataflow.joinbug;

import com.google.api.services.datastore.DatastoreV1;
import com.google.cloud.dataflow.sdk.transforms.DoFn;
import com.google.cloud.dataflow.sdk.values.KV;

/**
 * Created by pieter on 02.06.15.
 */
public class MakeKVFromParent extends DoFn<DatastoreV1.Entity, KV<Long, DatastoreV1.Entity>> {

    @Override
    public void processElement(ProcessContext processContext) throws Exception {
        DatastoreV1.Entity element = processContext.element();
        int pathElementCount = element.getKey().getPathElementCount();
        if(pathElementCount > 1){
            DatastoreV1.Key.PathElement pathElement = element.getKey().getPathElement(pathElementCount - 2);
            processContext.output(KV.of(pathElement.getId(), element));
        } else if (pathElementCount == 1){
            DatastoreV1.Key.PathElement pathElement = element.getKey().getPathElement(0);
            processContext.output(KV.of(pathElement.getId(), element));
        }
    }
}
