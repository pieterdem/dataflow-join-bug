package dataflow.joinbug;

import com.google.api.client.auth.oauth2.Credential;
import com.google.api.client.http.HttpRequestInitializer;
import com.google.api.services.datastore.DatastoreV1;
import com.google.api.services.datastore.client.Datastore;
import com.google.api.services.datastore.client.DatastoreException;
import com.google.api.services.datastore.client.DatastoreFactory;
import com.google.api.services.datastore.client.DatastoreHelper;
import com.google.cloud.dataflow.sdk.Pipeline;
import com.google.cloud.dataflow.sdk.io.DatastoreIO;
import com.google.cloud.dataflow.sdk.options.GcpOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptions;
import com.google.cloud.dataflow.sdk.options.PipelineOptionsFactory;
import com.google.cloud.dataflow.sdk.transforms.ParDo;
import com.google.cloud.dataflow.sdk.transforms.join.CoGbkResult;
import com.google.cloud.dataflow.sdk.transforms.join.CoGroupByKey;
import com.google.cloud.dataflow.sdk.transforms.join.KeyedPCollectionTuple;
import com.google.cloud.dataflow.sdk.util.RetryHttpRequestInitializer;
import com.google.cloud.dataflow.sdk.values.KV;
import com.google.cloud.dataflow.sdk.values.PCollection;
import com.google.cloud.dataflow.sdk.values.TupleTag;

/**
 * Hello world!
 *
 */
public class Example
{
    private static final String DATASET_ID = "certain-nexus-865";

    private static void addData(Datastore datastore) throws DatastoreException {
        DatastoreV1.Entity.Builder entityBuilder = DatastoreV1.Entity.newBuilder();
        entityBuilder.setKey(DatastoreHelper.makeKey("Entity1", 1L));
        addEntity(datastore, entityBuilder.build());

        entityBuilder = DatastoreV1.Entity.newBuilder();
        entityBuilder.setKey(DatastoreHelper.makeKey("Entity1", 1L, "Entity2", 2L));
        addEntity(datastore, entityBuilder.build());

        entityBuilder = DatastoreV1.Entity.newBuilder();
        entityBuilder.setKey(DatastoreHelper.makeKey("Entity2", 2L, "Entity3", 3L));
        addEntity(datastore, entityBuilder.build());
    }

    private static void addEntity(Datastore datastore, DatastoreV1.Entity entity) throws DatastoreException {
        DatastoreV1.BeginTransactionRequest.Builder beginTxn = DatastoreV1.BeginTransactionRequest.newBuilder();
        DatastoreV1.BeginTransactionResponse txn = datastore.beginTransaction(beginTxn.build());

        DatastoreV1.ReadOptions.Builder readOpts = DatastoreV1.ReadOptions.newBuilder().setTransaction(txn.getTransaction());
        DatastoreV1.LookupRequest.Builder lookup =
                DatastoreV1.LookupRequest.newBuilder().addKey(entity.getKey()).setReadOptions(readOpts);
        DatastoreV1.LookupResponse response = datastore.lookup(lookup.build());
        if(response.getFoundCount() == 0) {
            DatastoreV1.CommitRequest.Builder commitRequestBuilder = DatastoreV1.CommitRequest.newBuilder();
            commitRequestBuilder.setTransaction(txn.getTransaction())
                    .setMutation(DatastoreV1.Mutation.newBuilder().addInsert(entity));

            datastore.commit(commitRequestBuilder.build());
        } else {
            DatastoreV1.CommitRequest.Builder commitRequestBuilder = DatastoreV1.CommitRequest.newBuilder();
            commitRequestBuilder.setTransaction(txn.getTransaction())
                    .setMutation(DatastoreV1.Mutation.newBuilder().addUpdate(entity));

            datastore.commit(commitRequestBuilder.build());
        }
    }

    public static Datastore getDatastore(PipelineOptions pipelineOptions, String datasetId){
        return getDatastore(pipelineOptions, "https://www.googleapis.com", datasetId);
    }

    public static Datastore getDatastore(PipelineOptions pipelineOptions, String host, String datasetId) {
        com.google.api.services.datastore.client.DatastoreOptions.Builder builder =
                (new com.google.api.services.datastore.client.DatastoreOptions.Builder())
                        .host(host)
                        .dataset(datasetId)
                        .initializer(new RetryHttpRequestInitializer((HttpRequestInitializer) null));

        Credential credential = ((GcpOptions)pipelineOptions.as(GcpOptions.class)).getGcpCredential();
        if(credential != null) {
            builder.credential(credential);
        }

        return DatastoreFactory.get().create(builder.build());
    }

    private static DatastoreV1.Query makeQueryForKind(String kind){
        DatastoreV1.Query.Builder query = DatastoreV1.Query.newBuilder();
        query.addKindBuilder().setName(kind);


        return query.build();
    }

    public static void main( String[] args ) throws DatastoreException {
        PipelineOptions pipelineOptions = PipelineOptionsFactory.fromArgs(args).withValidation().as(PipelineOptions.class);
        Pipeline pipeline = Pipeline.create(pipelineOptions);
        Datastore datastore = getDatastore(pipelineOptions, DATASET_ID);

        addData(datastore);

        PCollection<KV<Long, DatastoreV1.Entity>> users = pipeline.apply(DatastoreIO.readFrom(DATASET_ID, makeQueryForKind("Entity1")))
                .apply(ParDo.of(new MakeKVFromParent()));
        PCollection<KV<Long, DatastoreV1.Entity>> locations = pipeline.apply(DatastoreIO.readFrom(DATASET_ID, makeQueryForKind("Entity2")))
                .apply(ParDo.of(new MakeKVFromParent()));
        PCollection<KV<Long, DatastoreV1.Entity>> cars = pipeline.apply(DatastoreIO.readFrom(DATASET_ID, makeQueryForKind("Entity3")))
                .apply(ParDo.of(new MakeKVFromParent()));

        TupleTag<DatastoreV1.Entity> carsTag = new TupleTag<DatastoreV1.Entity>();
        PCollection<KV<Long, CoGbkResult>> groupedCars = KeyedPCollectionTuple.of(carsTag, cars)
                .apply(CoGroupByKey.<Long>create());

        TupleTag<CoGbkResult> groupedCarsTag = new TupleTag<CoGbkResult>();
        TupleTag<DatastoreV1.Entity> locationsTag = new TupleTag<DatastoreV1.Entity>();
        PCollection<KV<Long, CoGbkResult>> locationData = KeyedPCollectionTuple.of(groupedCarsTag, groupedCars)
                .and(locationsTag, locations)
                .apply(CoGroupByKey.<Long>create());

        //Comment this block of code to remove the bug.
        TupleTag<CoGbkResult> locationDataTag = new TupleTag<CoGbkResult>();
        TupleTag<DatastoreV1.Entity> usersTag = new TupleTag<DatastoreV1.Entity>();
        PCollection<KV<Long, CoGbkResult>> userData = KeyedPCollectionTuple.of(locationDataTag, locationData)
                .and(usersTag, users)
                .apply(CoGroupByKey.<Long>create());

        //Do some computation for each user.
        pipeline.run();
    }
}
